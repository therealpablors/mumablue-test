<?php

class Checkout {
  private $rules = [];
  private $products = [
    "GR1" => [
      "name" => "Green tea",
      "price" => 3.11,
      "currency" => "£"
    ],
    "SR1" => [
      "name" => "Strawberries",
      "price" => 5.00,
      "currency" => "£"
    ],
    "CF1" => [
      "name" => "Coffee",
      "price" => 11.23,
      "currency" => "£"
    ]
  ];
  private $basket = [];
  private $total = 0;

  public function __construct(array $rules) {
    if (gettype($rules) === 'array') {
      $this->rules = $rules;
    } else {
      throw new TypeError('Rules must be an array');
    }
  }

  public function getRules() {
    return $this->rules;
  }

  public function getProducts() {
    return $this->products;
  }

  public function getBasket() {
    return $this->basket;
  }

  public function getTotal() {
    return $this->total;
  }

  public function setTotal($total) {
    $this->total = $total;
  }

  public function scan(string $item) {
    array_push($this->basket, $item);
    $this->calculateTotal();
  }

  public function getProductByName(string $name) {
    $products = $this->getProducts();
    if (array_key_exists($name, $products)) {
      return [$name => $products[$name]];
    } else {
      return [];
    }
  }

  private function calculateTotal() {
    $basket = $this->getBasket();
    $items = array_count_values($basket);
    $products = $this->getProducts();
    $totalPrice = 0;
    foreach($items as $product => $quantity) {
      $totalPrice = $totalPrice + $this->applyRules($product, $quantity, $products[$product]['price']);
    }
    $this->setTotal($totalPrice);
  }

  private function applyRules($product, $quantity, $price) {
    $rules = $this->getRules();
    foreach($rules as $rule => $data) {
      switch($rule) {
        case 'buy-one-get-one-free':
          $values = array_count_values($data['products']);
          if (array_key_exists($product, $values)) {
            $freeProducts = round(($quantity/2),0,PHP_ROUND_HALF_DOWN);
            return ($quantity - $freeProducts) * $price;
          }
        break;
        case 'bulk-discount':
          $values = array_count_values($data['products']);
          if (array_key_exists($product, $values)) {
            if($quantity >= $data['number']) {
              return $data['new_price'] * $quantity;
            } else {
              return $quantity * $price;
            }
          }
        break;
      }
    }
    return $quantity * $price;
  }
}
