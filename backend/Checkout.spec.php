<?php

use PHPUnit\Framework\TestCase;

require './Checkout.php';

/*
* @covers Checkout
*/

class CheckoutTest extends TestCase {

  protected $checkout;
  protected $products;
  protected $rules;
  protected $basket;
  protected $total;

  protected function setUp() {
    $this->checkout = new Checkout([]);
    $this->products = $this->checkout->getProducts();
    $this->rules = $this->checkout->getRules();
    $this->basket = $this->checkout->getBasket();
    $this->total = $this->checkout->getTotal();
  }

  /** @test */
  public function itShouldHaveRulesEmpty() {
    $this->assertEquals([], $this->rules);
  }

  /** @test
  * @expectedException TypeError
  */
  public function itShouldThrowAnExceptionIfRulesAreWrong() {
    $co = new Checkout('');
  }

  /** @test */
  public function itShouldHaveRulesSetted() {
    $rules = [
      'buy-one-get-one-free' => [
        'products' => ['GR1']
      ],
      'bulk-discount' => [
        'products' => ['SR1'],
        'number' => 3,
        'new_price' => 4.50
      ]
    ];
    $co = new Checkout($rules);
    $this->assertEquals($rules, $co->getRules());
  }

  /** @test */
  public function itShouldReturnBasketEmpty() {
    $this->assertEquals([], $this->basket);
  }

  /** @test */
  public function itShouldInsertItemToBasket() {
    $this->assertEquals([], $this->basket);
    $this->checkout->scan('GR1');
    $this->assertEquals(['GR1'], $this->checkout->getBasket());
  }

  /** @test */
  public function itShouldHaveTotal0ByDefault() {
    $this->assertEquals(0, $this->total);
  }

  /** @test */
  public function itShouldHaveThreeProductsByDefault() {
    $this->assertArrayHasKey('GR1', $this->products);
    $this->assertArrayHasKey('SR1', $this->products);
    $this->assertArrayHasKey('CF1', $this->products);
  }

  /** @test */
  public function productGR1ShouldHaveNamePriceAndCurrencyProperties() {
    $product = $this->products['GR1'];
    $this->assertArrayHasKey('name', $product);
    $this->assertArrayHasKey('price', $product);
    $this->assertArrayHasKey('currency', $product);
    $this->assertEquals($product['name'], 'Green tea');
    $this->assertEquals($product['price'], 3.11);
    $this->assertEquals($product['currency'], '£');
  }

  /** @test */
  public function productSR1ShouldHaveNamePriceAndCurrencyProperties() {
    $product = $this->products['SR1'];
    $this->assertArrayHasKey('name', $product);
    $this->assertArrayHasKey('price', $product);
    $this->assertArrayHasKey('currency', $product);
    $this->assertEquals($product['name'], 'Strawberries');
    $this->assertEquals($product['price'], 5.00);
    $this->assertEquals($product['currency'], '£');
  }

  /** @test */
  public function productCF1ShouldHaveNamePriceAndCurrencyProperties() {
    $product = $this->products['CF1'];
    $this->assertArrayHasKey('name', $product);
    $this->assertArrayHasKey('price', $product);
    $this->assertArrayHasKey('currency', $product);
    $this->assertEquals($product['name'], 'Coffee');
    $this->assertEquals($product['price'], 11.23);
    $this->assertEquals($product['currency'], '£');
  }

  /** @test */
  public function itShouldReturnProductIfItExist() {
    $productName = 'CF1';
    $product = $this->checkout->getProductByName($productName);
    $this->assertTrue(array_key_exists($productName, $product));
  }

  /** @test */
  public function itShoulNotdReturnProductIfItNotExist() {
    $productName = 'CF12';
    $product = $this->checkout->getProductByName($productName);
    $this->assertFalse(array_key_exists($productName, $product));
  }

  /** @test */
  public function TestDataCase1() {
    $rules = [
      'buy-one-get-one-free' => [
        'products' => ['GR1']
      ],
      'bulk-discount' => [
        'products' => ['SR1'],
        'number' => 3,
        'new_price' => 4.50
      ]
    ];
    $co = new Checkout($rules);
    $co->scan('GR1');
    $co->scan('SR1');
    $co->scan('GR1');
    $co->scan('GR1');
    $co->scan('CF1');
    $this->assertEquals($co->getBasket(), ['GR1', 'SR1', 'GR1', 'GR1', 'CF1']);
    $this->assertEquals($co->getTotal(), 22.45);
  }

  /** @test */
  public function TestDataCase2() {
    $rules = [
      'buy-one-get-one-free' => [
        'products' => ['GR1']
      ],
      'bulk-discount' => [
        'products' => ['SR1'],
        'number' => 3,
        'new_price' => 4.50
      ]
    ];
    $co = new Checkout($rules);
    $co->scan('GR1');
    $co->scan('GR1');
    $this->assertEquals($co->getBasket(), ['GR1', 'GR1']);
    $this->assertEquals($co->getTotal(), 3.11);
  }

  /** @test */
  public function TestDataCase3() {
    $rules = [
      'buy-one-get-one-free' => [
        'products' => ['GR1']
      ],
      'bulk-discount' => [
        'products' => ['SR1'],
        'number' => 3,
        'new_price' => 4.50
      ]
    ];
    $co = new Checkout($rules);
    $co->scan('SR1');
    $co->scan('SR1');
    $co->scan('GR1');
    $co->scan('SR1');
    $this->assertEquals($co->getBasket(), ['SR1', 'SR1', 'GR1', 'SR1']);
    $this->assertEquals($co->getTotal(), 16.61);
  }
}
