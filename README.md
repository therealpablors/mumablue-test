# Mumablue Test #

Mumable are looking for new developer.

### What is this repository for? ###

This repository have all the code for frontend and backend tests.

### How do I get set up? ###

Frontend:

You have an index.html file into frontend folder that is the result of mockups. This index.html render a mockup page for desktop version and mobile version.

* css folder: main.css to customisize index.html like mockups
* js folder: 
	* offcanvas.js: Custom library to add offcanvas functionality to bootstrap navbar panel.

Backend:

* phpunit is neccesary to run all the tests. If you have phpunit installed in your machine you can run tests by:
`phpunit Checkout.spec.php`