(function(){
  var offcanvasElement = document.querySelector('button[data-toggle=offcanvas]');
  var open = false;
  var panel = document.querySelector(offcanvasElement.getAttribute('data-target'));
  var offcanvasElementOriginalStyle = offcanvasElement.style;

  function closePanel() {
    panel.style.width = '0';
    offcanvasElement.style = offcanvasElementOriginalStyle;
  }

  function openPanel() {
    panel.style.width = '250px';
    offcanvasElement.style.marginRight = '250px';
  }

  offcanvasElement.onclick = function onClick() {
    if (open) {
      closePanel();
      open = false;
    } else {
      openPanel();
      open = true;
    }
  }

}());
